from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

# Set dynamic db location
SQLALCHEMY_DATABASE_URI = 'sqlite:////Users/aaptekmann/Desktop/synvep/synvep_flask/toy1.db'

# Create session
engine = create_engine(SQLALCHEMY_DATABASE_URI)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


